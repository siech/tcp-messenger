/*
 * File:   messenger_client.c
 * Author: akhila vegesna
 *
 * Created on November 8, 2016, MAX_CLIENTS:28 AM
 */
/*
 * htons() host to network short
 * htonl() host to network long
 * ntohs() network to host short
 * ntohl() network to host long
 */

#include <stdio.h> /* printf */
#include <string.h>    /* strlen */
#include <sys/socket.h>    /* socket */
#include <arpa/inet.h>
#include <stdlib.h> /* inet_addr */
#include <unistd.h>
#include <assert.h>
#include <time.h>
#include <sys/types.h>
#include <ctype.h>
#include <unistd.h>    /* write */
#include <pthread.h> /* for threading , link with pthread */
/* handles signal such as SIGINT */
#include <signal.h>
/* for polling */
#include <poll.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <crypt.h>
#include <rpc/types.h>

#define USAGE_REG 1
#define USAGE_UNR -1
#define USAGE_REQ 0
#define USAGE_LOG_IN 2
#define USAGE_LOG_OUT -2
#define USAGE_I -3
#define USAGE_IA 3
#define UN_AVL 200
#define UN_UNAVL 500
#define REG_SUC 25
#define REG_FAIL -25
#define LOG_SUC 26
#define LOG_FAIL -26
#define CLIENT_SERV_PORT 5100
#define MAX_CLIENTS 10

int control_handler();
void *inviter(void *);
void *requester(void *);
void *terminator(void *);
void *reply_handler(void *);
void *chat_initiator(void *);
void *invite_acceptor(void *);
void *server_consultant(void *);

enum msg_type {
    MESSAGE, INVITE, INVITE_ACCEPT, CLIENTS, OTHER
};

struct clients {
    int p;
    char ip[20];
    char username[20];
    char password[30];
    int extra_inf;
    char friends_uns[MAX_CLIENTS][20];
};

/**
 * Registration details
 */
struct reg_data {
    int port;
    int usage;
    char username[20];
    char password[30];
    char p_f_un[20];
    char msg[2048];
};

struct config {
    char keyword[20];
    char value[20];
};

struct message {
    char msg[2048];
    char sender_un[20];
    enum msg_type type;
};

struct config conf[5];
struct reg_data reg_inf[1];
struct clients registered[MAX_CLIENTS];
struct message msg_struct[1];
struct sockaddr_in this_client;

char conf_file_n[20];
int usage, sock_d_to_create_s_conn, sock_d_to_accept_p_conn, opt_shown = FALSE;
int this_port_no = 0, sock_created = FALSE, list_shown = FALSE;
int client_server_p, friends_sock_d[MAX_CLIENTS];
char msg[2048] = "", friend_un[20];

/* set of friends socket descriptors */
fd_set readfds;

void sigHandler(int dummy) {
    puts("\n\n  ****************************************************************\n"
            "\t\tSomeone ctrl c'd. Exiting gracefully\n"
            "  ****************************************************************\n");
    int *n;
    n = malloc(1);
    *n = sock_d_to_create_s_conn;
    pthread_t sniffer_thread;
    if (pthread_create(&sniffer_thread, NULL, terminator, (void *) n) < 0) {
        perror("Could not create terminator");
        exit(EXIT_SUCCESS);
    }

    /* Now join the thread , so that we dont terminate before the thread */
    pthread_join(sniffer_thread, NULL);

    close(sock_d_to_create_s_conn);
    sock_created = FALSE;

    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {

    if (argc != 2) {
        puts("\nusage: messenger_client configuration_file\n");
        return EXIT_FAILURE;
    }

    FILE *conf_file;
    char temp_val[20];

    /* defer control to sigHandler to prevent ctrl+c default action */
    signal(SIGINT, sigHandler);

    strncpy(conf_file_n, argv[1], strlen(argv[1]));

    conf_file = fopen(conf_file_n, "r");
    if (conf_file == NULL) {
        printf("Couldn't open %s for reading.\n", conf_file_n);
        perror("");
        exit(0);
    }

    int repeat = 1;
    int i;
    while (repeat) {
        if ((fscanf(conf_file, "%s\n", temp_val)) != 1) {
            repeat = 0;
            break;
        }

        const char delim[2] = ":";
        char *token;

        /* get the first token; keyword */
        token = strtok(temp_val, delim);

        /* server host is at index 0 */
        if (strcmp(token, "servhost") == 0)i = 0;
            /* server host is at index 1 */
        else if (strcmp(token, "servport") == 0)i = 1;

        strncpy(conf[i].keyword, token, strlen(token));

        /* walk through the next token; value */
        token = strtok(NULL, delim);
        strncpy(conf[i].value, token, strlen(token));
    }

    /* close the configuration file */
    fclose(conf_file);

    /* set socket descriptors of friends to 0 */
    memset(friends_sock_d, 0, MAX_CLIENTS);

    /* Sock used in creating conn to central server */
    sock_d_to_create_s_conn = socket(AF_INET, SOCK_STREAM, 0);
    sock_d_to_create_s_conn = sock_d_to_create_s_conn;
    if (sock_d_to_create_s_conn == -1) {
        printf("\nCould not create socket");
        exit(EXIT_FAILURE);
    }

    /* Initialize random number generator */
    srand((unsigned) time(NULL));

    /* Generate a random port number */
    this_port_no = rand() % 15000 + 10000;

    /* Local */
    memset(&this_client, 0, sizeof (struct sockaddr_in));
    this_client.sin_family = AF_INET;
    this_client.sin_port = htons(this_port_no);
    this_client.sin_addr.s_addr = INADDR_ANY;

    printf("\nSocket created at port %d\n", this_port_no);


    /* Initialize random number generator */
    srand((unsigned) time(NULL));

    /* Generate a random port number for client's server */
    client_server_p = rand() % 7000 + 70000;

    /* Sock used in accepting conn from friend */
    sock_d_to_accept_p_conn = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_d_to_accept_p_conn == -1) {
        printf("\nCould not create socket");
    }

    /* client server port */
    memset(&this_client, 0, sizeof (struct sockaddr_in));
    this_client.sin_family = AF_INET;
    /* commented to allow for random port while using a single machine for testing  */
    /*   this_client.sin_port = htons(CLIENT_SERV_PORT); */
    this_client.sin_port = htons(client_server_p);
    this_client.sin_addr.s_addr = INADDR_ANY;

    /* Bind to client server port */
    if (bind(sock_d_to_accept_p_conn, (struct sockaddr *) &this_client, sizeof (struct sockaddr)) < 0) {
        perror("Bind failure. Error");
        exit(EXIT_FAILURE);
    }

    /* call the control handler */
    control_handler();

    if (usage != USAGE_LOG_OUT) {

        memset(&registered, 0, sizeof (registered));

        /* Receive a list of friends from the central server */
        recv(sock_d_to_create_s_conn, registered, sizeof (registered), 0);
    }
    return EXIT_SUCCESS;
}

void *server_consultant(void *n) {
    struct sockaddr_in central_server;

    /* Central server */
    memset(&central_server, 0, sizeof (struct sockaddr_in));
    central_server.sin_family = AF_INET;
    central_server.sin_port = htons(atoi(conf[1].value));
    central_server.sin_addr.s_addr = inet_addr(conf[0].value);

    /* puts("\nConnected to central server"); */
    if (usage != USAGE_LOG_OUT) {

        memset(&reg_inf, 0, sizeof (struct reg_data));
        reg_inf[0].port = client_server_p;
        reg_inf[0].usage = usage;

        char tmp_un[20];
        char *tmp_pw;
        char salt[] = "00";
        printf("Username(max 20): ");
        scanf("%s", tmp_un);
        tmp_pw = crypt(getpass("Password(max 20): "), salt);
        strncpy(reg_inf[0].username, tmp_un, strlen(tmp_un));
        strncpy(reg_inf[0].password, tmp_pw, strlen(tmp_pw));
    } else {
        reg_inf[0].usage = usage;
    }

    /* close socket communicating with central server */
    close(sock_d_to_create_s_conn);
    sock_created = FALSE;
    struct sockaddr_in client;

    if (!sock_created) {
        /* Sock used in creating conn to central server */
        sock_d_to_create_s_conn = socket(AF_INET, SOCK_STREAM, 0);
        if (sock_d_to_create_s_conn == -1) {
            printf("\nCould not create socket");
        }

        /* Local */
        memset(&client, 0, sizeof (struct sockaddr_in));
        client.sin_family = AF_INET;
        client.sin_port = htons(this_port_no);
        client.sin_addr.s_addr = INADDR_ANY;

        sock_created = TRUE;
    }

    connect(sock_d_to_create_s_conn, (struct sockaddr *) &central_server, sizeof (central_server));

    send(sock_d_to_create_s_conn, reg_inf, sizeof (struct reg_data), 0);

    if (usage != USAGE_LOG_OUT) {

        memset(&registered, 0, sizeof (registered));

        /* Receive a list of friends from the central server */
        recv(sock_d_to_create_s_conn, registered, sizeof (registered), 0);
    }

    /* close connection to server */
    close(sock_d_to_create_s_conn);
    sock_created = FALSE;

    if (usage == USAGE_REG)
        puts("\n\tRegistration request made");
    else if (usage == USAGE_LOG_IN)
        puts("\n\tLogin request made");
    else if (usage == USAGE_LOG_OUT) {
        puts("\n\tLogout request made");

        /* return control to the control handler */
        control_handler();
    }
}

void *terminator(void *n) {

    struct sockaddr_in central_server;

    /* Central server */
    memset(&central_server, 0, sizeof (struct sockaddr_in));
    central_server.sin_family = AF_INET;
    central_server.sin_port = htons(atoi(conf[1].value));
    central_server.sin_addr.s_addr = inet_addr(conf[0].value);

    /* close socket communicating with central server */
    close(sock_d_to_create_s_conn);
    sock_created = FALSE;
    struct sockaddr_in client;

    if (!sock_created) {
        /* Sock used in creating conn to central server */
        sock_d_to_create_s_conn = socket(AF_INET, SOCK_STREAM, 0);
        if (sock_d_to_create_s_conn == -1) {
            printf("\nCould not create socket");
        }

        /* Local */
        memset(&client, 0, sizeof (struct sockaddr_in));
        client.sin_family = AF_INET;
        client.sin_port = htons(this_port_no);
        client.sin_addr.s_addr = INADDR_ANY;

        sock_created = TRUE;
    }

    /* Connect to the central server */
    connect(sock_d_to_create_s_conn, (struct sockaddr *) &central_server, sizeof (central_server));

    reg_inf[0].usage = USAGE_UNR;
    send(sock_d_to_create_s_conn, reg_inf, sizeof (struct reg_data), 0);

    /* close connection to server */
    close(sock_d_to_create_s_conn);
    sock_created = FALSE;

}

void *requester(void *n) {

    struct sockaddr_in central_server;

    /* Central server */
    memset(&central_server, 0, sizeof (struct sockaddr_in));
    central_server.sin_family = AF_INET;
    central_server.sin_port = htons(atoi(conf[1].value));
    central_server.sin_addr.s_addr = inet_addr(conf[0].value);

    /* close socket communicating with central server */
    close(sock_d_to_create_s_conn);
    sock_created = FALSE;
    struct sockaddr_in client;

    if (!sock_created) {
        /* Sock used in creating conn to central server */
        sock_d_to_create_s_conn = socket(AF_INET, SOCK_STREAM, 0);
        if (sock_d_to_create_s_conn == -1) {
            printf("\nCould not create socket");
        }

        /* Local */
        memset(&client, 0, sizeof (struct sockaddr_in));
        client.sin_family = AF_INET;
        client.sin_port = htons(this_port_no);
        client.sin_addr.s_addr = INADDR_ANY;

        sock_created = TRUE;
    }

    /* Connect to the central server */
    connect(sock_d_to_create_s_conn, (struct sockaddr *) &central_server, sizeof (central_server));

    reg_inf[0].usage = USAGE_REQ;

    send(sock_d_to_create_s_conn, reg_inf, sizeof (struct reg_data), 0);

    memset(&registered, 0, sizeof (registered));

    /* Receive a list of friends from the central server */
    recv(sock_d_to_create_s_conn, registered, sizeof (registered), 0);

    /* close connection to server */
    close(sock_d_to_create_s_conn);
    sock_created = FALSE;
}

void *inviter(void *n) {
    int i = 0, j = 0, friend_p = 0, friend_already = FALSE;
    char friend_ip[20];
    struct sockaddr_in client;
    struct sockaddr_in central_server;

    reg_inf[0].usage = USAGE_I;
    for (i = 0; i < MAX_CLIENTS; i++) {
        if (strcmp(registered[i].ip, "") == 0 || strcmp(registered[i].ip, "0.0.0.0") == 0) {
            i++;
            continue;
        }

        if (strcmp(registered[i].username, friend_un) == 0) {
            for (j = 0; j < MAX_CLIENTS; j++) {
                if (strcmp(registered[i].friends_uns[j], friend_un) == 0) {
                    friend_already = TRUE;
                    break;
                }
                j++;
            }
            if (!friend_already) {
                friend_p = registered[i].p;
                strncpy(friend_ip, registered[i].ip, sizeof registered[i].ip);
            }
            break;
        }
    }

    if (friend_p == 0) {
        puts("User does not exist or is your friend already\n");
    } else {
        /* clear message in memory */
        memset(reg_inf[0].msg, 0, sizeof reg_inf[0].msg);

        /* prepare invite details */
        strncpy(reg_inf[0].msg, msg, sizeof msg);
        strncpy(reg_inf[0].p_f_un, friend_un, sizeof friend_un);

        /* Central server */
        memset(&central_server, 0, sizeof (struct sockaddr_in));
        central_server.sin_family = AF_INET;
        central_server.sin_port = htons(atoi(conf[1].value));
        central_server.sin_addr.s_addr = inet_addr(conf[0].value);

        /* close socket communicating with central server */
        close(sock_d_to_create_s_conn);
        sock_created = FALSE;

        if (!sock_created) {
            /* Sock used in creating conn to central server */
            sock_d_to_create_s_conn = socket(AF_INET, SOCK_STREAM, 0);
            if (sock_d_to_create_s_conn == -1) {
                printf("\nCould not create socket");
            }

            /* Local */
            memset(&client, 0, sizeof (struct sockaddr_in));
            client.sin_family = AF_INET;
            client.sin_port = htons(this_port_no);
            client.sin_addr.s_addr = INADDR_ANY;

            sock_created = TRUE;
        }

        /* Connect to the central server */
        connect(sock_d_to_create_s_conn, (struct sockaddr *) &central_server, sizeof (central_server));

        /* send invite details */
        send(sock_d_to_create_s_conn, reg_inf, sizeof (struct reg_data), 0);

        /* close socket communicating with central server */
        close(sock_d_to_create_s_conn);
        sock_created = FALSE;
    }

}

void *invite_acceptor(void *n) {
    int i = 0, j = 0, friend_p = 0, friend_already = FALSE;
    char friend_ip[20];
    struct sockaddr_in client;
    struct sockaddr_in central_server;

    reg_inf[0].usage = USAGE_IA;
    for (i = 0; i < MAX_CLIENTS; i++) {
        if (strcmp(registered[i].ip, "") == 0 || strcmp(registered[i].ip, "0.0.0.0") == 0) {
            continue;
        }
        if (strcmp(registered[i].username, friend_un) == 0) {
            for (j = 0; j < MAX_CLIENTS; j++) {
                if (strcmp(registered[i].friends_uns[j], friend_un) == 0) {
                    friend_already = TRUE;
                    break;
                }
                j++;
            }
            if (!friend_already) {
                friend_p = registered[i].p;
                strncpy(friend_ip, registered[i].ip, sizeof registered[i].ip);
            }
            break;
        }
    }

    if (friend_p == 0) {
        puts("User does not exist or is your friend already\n");
    } else {
        /* clear message in memory */
        memset(reg_inf[0].msg, 0, sizeof reg_inf[0].msg);

        /* prepare invite details */
        strncpy(reg_inf[0].msg, msg, sizeof msg);
        strncpy(reg_inf[0].p_f_un, friend_un, sizeof friend_un);

        /* Central server */
        memset(&central_server, 0, sizeof (struct sockaddr_in));
        central_server.sin_family = AF_INET;
        central_server.sin_port = htons(atoi(conf[1].value));
        central_server.sin_addr.s_addr = inet_addr(conf[0].value);

        if (!sock_created) {
            /* Sock used in creating conn to central server */
            sock_d_to_create_s_conn = socket(AF_INET, SOCK_STREAM, 0);
            if (sock_d_to_create_s_conn == -1) {
                printf("\nCould not create socket");
            }

            /* Local */
            memset(&client, 0, sizeof (struct sockaddr_in));
            client.sin_family = AF_INET;
            client.sin_port = htons(this_port_no);
            client.sin_addr.s_addr = INADDR_ANY;

            sock_created = TRUE;
        }

        /* Connect to the central server */
        connect(sock_d_to_create_s_conn, (struct sockaddr *) &central_server, sizeof (central_server));

        /* send accept invite details */
        send(sock_d_to_create_s_conn, reg_inf, sizeof (struct reg_data), 0);

        /* close socket communicating with central server */
        close(sock_d_to_create_s_conn);
        sock_created = FALSE;
    }

}

void *chat_initiator(void *n) {
    /* Get the socket descriptor */
    int i = 0, j = 0, k = 0, friend_p = 0;
    struct sockaddr_in friend;
    char friend_ip[20];
    int sock_d_to_create_p_conn;

    for (i = 0; i < MAX_CLIENTS; i++) {
        /* if index of registered has a user */
        if (strcmp(registered[i].ip, "") == 0 || strcmp(registered[i].ip, "0.0.0.0") == 0) {
            continue;
        }
        /* if current user is receiver of the message */
        if (strcmp(registered[i].username, friend_un) == 0) {
            for (j = 0; j < MAX_CLIENTS; j++) {
                /* if current user is sender of message */
                if (strcmp(registered[j].username, reg_inf[0].username) == 0) {
                    for (k = 0; k < MAX_CLIENTS; k++) {
                        /* if the sender of message has the receiver as a friend */
                        if (strcmp(registered[j].friends_uns[k], "") != 0 && strcmp(registered[j].friends_uns[k], friend_un) == 0) {
                            friend_p = registered[i].p;
                            strncpy(friend_ip, registered[i].ip, sizeof registered[i].ip);
                            break;
                        }
                    }
                }
                if (friend_p != 0)break;
            }
            break;
        }
    }

    if (friend_p == 0) {
        puts("Friend does not exist or the user is not your friend yet\n");
    } else {

        /* Sock used in creating conn to friend */
        sock_d_to_create_p_conn = socket(AF_INET, SOCK_STREAM, 0);
        if (sock_d_to_create_p_conn == -1) {
            printf("\nCould not create socket");
        }

        /* Initialize random number generator */
        srand((unsigned) time(NULL));

        /* Generate a random port number */
        int port = rand() % 7000 + 70000;

        /* Details of socket to create conn to friend */
        memset(&this_client, 0, sizeof (struct sockaddr_in));
        this_client.sin_family = AF_INET;
        this_client.sin_port = htons(port);
        this_client.sin_addr.s_addr = INADDR_ANY;

        /* Peer */
        memset(&friend, 0, sizeof (struct sockaddr_in));
        friend.sin_family = AF_INET;
        friend.sin_port = htons(friend_p);
        friend.sin_addr.s_addr = inet_addr(friend_ip);

        /* Connect to the friend */
        if (connect(sock_d_to_create_p_conn, (struct sockaddr *) &friend, sizeof (friend)) < 0) {
            perror("connect failed. Error");
            exit(0);
        } else {
            /*            puts("\nConnected to friend"); */
        }

        /* clear message in memory */
        memset(msg_struct[0].msg, 0, sizeof msg_struct[0].msg);

        /* prepare message */
        msg_struct[0].type = MESSAGE;
        strncpy(msg_struct[0].msg, msg, sizeof msg);
        strncpy(msg_struct[0].sender_un, reg_inf[0].username, sizeof reg_inf[0].username);

        /* send the message to friend */
        write(sock_d_to_create_p_conn, msg_struct, sizeof msg_struct);

        /* close connection to friend */
        close(sock_d_to_create_p_conn);
        shutdown(sock_d_to_create_p_conn, 2);
    }
}

void *reply_handler(void *sock_d) {
    int sock_d_for_p, c, *new_sock, activity;
    struct sockaddr_in friend;
    int i, max_sock_d, go_on = FALSE;

    /* Listen */
    listen(sock_d_to_accept_p_conn, 3);

    struct timeval tv;
    tv.tv_sec = 0; /* seconds */
    tv.tv_usec = 500; /* microseconds */

    /* Change the socket into non-blocking state */
    fcntl(sock_d_to_accept_p_conn, F_SETFL, O_NONBLOCK);

    /* clear the socket descriptor set */
    FD_ZERO(&readfds);

    /* add this client server socket descriptor to the socket descriptor set */
    FD_SET(sock_d_to_accept_p_conn, &readfds);

    /* add friends sockets to the set */
    for (i = 0; i < MAX_CLIENTS; i++) {
        /* current socket descriptor */
        sock_d_for_p = friends_sock_d[i];

        /* if valid socket descriptor then add to read list */
        if (sock_d_for_p > 0) FD_SET(sock_d_for_p, &readfds);

        /* highest socket descriptor number, need it for the select function */
        if (sock_d_for_p > max_sock_d) max_sock_d = sock_d_for_p;
    }

    /* wait for an activity on one of the sockets , timeout is defined above */
    tv.tv_usec = 500; /* microseconds */
    activity = select(max_sock_d + 1, &readfds, NULL, NULL, &tv);

    if ((activity < 0) && (errno != EINTR)) {
        /* printf("select error"); */
    }

    if ((sock_d_for_p = accept(sock_d_to_accept_p_conn, (struct sockaddr *) &friend, (socklen_t*) & c)) > 0) {
        new_sock = malloc(1);
        *new_sock = sock_d_for_p;

        /* add new socket descriptor to array of sockets */
        for (i = 0; i < MAX_CLIENTS; i++) {
            /* if position is empty */
            if (friends_sock_d[i] == 0) {
                friends_sock_d[i] = sock_d_for_p;
                break;
            }
        }

        /* Receive message from the friend */
        if (recv(sock_d_for_p, msg_struct, sizeof (struct message), 0) > 0) {
            /* print the message */
            switch (msg_struct[0].type) {
                case MESSAGE:
                    printf("\n%20s >> %s\n\n", msg_struct[0].sender_un, msg_struct[0].msg);
                    break;
                case INVITE:
                    printf("\n[CHAT INVITE] %15s >> %s\n\n", msg_struct[0].sender_un, msg_struct[0].msg);
                    break;
                case INVITE_ACCEPT:
                    printf("\n[INVITE ACCEPT] %15s >> %s\n\n", msg_struct[0].sender_un, msg_struct[0].msg);
                    break;
                case CLIENTS:
                    /* Receive a list of friends from the central server */
                    memset(registered, 0, sizeof registered);
                    recv(sock_d_for_p, registered, sizeof (registered), 0);
                    list_shown = FALSE;
                    break;
                case OTHER:
                    /* Receive a list of friends from the central server */
                    memset(registered, 0, sizeof registered);
                    recv(sock_d_for_p, registered, sizeof (registered), 0);

                    break;
            }
        }

    }
}

int control_handler() {
    int *n;
    char opt[MAX_CLIENTS];
    n = malloc(1);
    *n = sock_d_to_create_s_conn;
    pthread_t sniffer_thread;
    struct sockaddr_in client;
    struct pollfd mypoll = {STDIN_FILENO, POLLIN | POLLPRI};

    for (;;) {
        printf("\nIssue a command:\n  l)\tLogin to server\n"
                "  r)\tRegister on server\n"
                "  exit) Exit\n");
        scanf("%s", opt);

        if (strcmp(opt, "r") == 0) {
            usage = USAGE_REG;
            break;
        } else if (strcmp(opt, "l") == 0) {
            usage = USAGE_LOG_IN;
            break;
        } else if (strcmp(opt, "exit") == 0) {
            int *n;
            n = malloc(1);
            *n = sock_d_to_create_s_conn;
            pthread_t sniffer_thread;
            if (pthread_create(&sniffer_thread, NULL, terminator, (void *) n) < 0) {
                perror("Could not create terminator");
                exit(EXIT_SUCCESS);
            }

            /* Now join the thread , so that we dont terminate before the thread */
            pthread_join(sniffer_thread, NULL);

            puts("\nGood bye!\n");
            /* close socket communicating with central server */
            close(sock_d_to_create_s_conn);
            sock_created = FALSE;

            exit(EXIT_SUCCESS);
        } else {
            puts("\nInvalid choice");
        }
    }

    if (pthread_create(&sniffer_thread, NULL, server_consultant, (void *) n) < 0) {
        perror("Could not create consultant");
        return EXIT_FAILURE;
    }

    /* Now join the thread , so that we dont terminate before the thread */
    pthread_join(sniffer_thread, NULL);

    /* close socket communicating with central server */
    close(sock_d_to_create_s_conn);
    sock_created = FALSE;

    if (!sock_created) {
        /* Sock used in creating conn to central server */
        sock_d_to_create_s_conn = socket(AF_INET, SOCK_STREAM, 0);
        if (sock_d_to_create_s_conn == -1) {
            printf("\nCould not create socket");
        }

        /* Local */
        memset(&client, 0, sizeof (struct sockaddr_in));
        client.sin_family = AF_INET;
        client.sin_port = htons(this_port_no);
        client.sin_addr.s_addr = INADDR_ANY;

        sock_created = TRUE;
    }

    char option[2048];
    char delim[2] = " ";
    char *token;
    char cmd[20];

    for (;;) {

        if (registered[0].extra_inf == UN_UNAVL) {
            puts("Username is unavailable. Kindly retry");

            if (pthread_create(&sniffer_thread, NULL, server_consultant, (void *) n) < 0) {
                perror("Could not create consultant");
                return EXIT_FAILURE;
            }

            /* Now join the thread , so that we dont terminate before the thread */
            pthread_join(sniffer_thread, NULL);

        } else if (registered[0].extra_inf == REG_FAIL) {

            puts("\t\tRegistration failed. Kindly retry\n");

            if (pthread_create(&sniffer_thread, NULL, server_consultant, (void *) n) < 0) {
                perror("Could not create consultant");
                return EXIT_FAILURE;
            }

            /* Now join the thread , so that we dont terminate before the thread */
            pthread_join(sniffer_thread, NULL);

        } else if (registered[0].extra_inf == LOG_FAIL) {

            puts("\t\tLogin failed. Kindly retry\n");

            if (pthread_create(&sniffer_thread, NULL, server_consultant, (void *) n) < 0) {
                perror("Could not create consultant");
                return EXIT_FAILURE;
            }

            /* Now join the thread , so that we dont terminate before the thread */
            pthread_join(sniffer_thread, NULL);

        } else {

            if (pthread_create(&sniffer_thread, NULL, reply_handler, (void*) n) < 0) {
                perror("Could not create reply handler");
                return EXIT_FAILURE;
            }
            pthread_join(sniffer_thread, NULL);

            if (!list_shown) {
                if (registered[0].extra_inf == REG_SUC)
                    puts("\t\tRegistration successful");
                else if (registered[0].extra_inf == LOG_SUC)
                    puts("\t\tLogin successful");

                int i;
                int title_shown = FALSE;
                int j = 1;
                for (i = 0; i < MAX_CLIENTS; i++) {
                    if (strcmp(registered[i].ip, "") == 0 || strcmp(registered[i].ip, "0.0.0.0") == 0 || strcmp(registered[i].username, reg_inf[0].username) == 0) {
                        continue;
                    }
                    if (!title_shown) {
                        puts("\nList of online users\n");
                        title_shown = TRUE;
                    }
                    printf("\t%d.%15s -> %s:%d\n", j++, registered[i].username, registered[i].ip, registered[i].p);
                }
                list_shown = TRUE;
            }

            memset(option, 0, sizeof (option));
            memset(cmd, 0, sizeof (cmd));
            memset(friend_un, 0, sizeof (friend_un));
            memset(msg, 0, sizeof (msg));
            if (!opt_shown) {
                printf("\n\nIssue a command:\n");
                printf("%29s%-13s\n%29s%-13s\n%29s%-13s\n%29s%-13s\n%29s%-13s\n%29s%-13s\n",
                        " m friend_username message) ", "Initiate chat",
                        " i potential_f_un message) ", "Invite friend",
                        " ia inviter_un message) ", "Accept invite",
                        " req) ", "Request for list of friends",
                        " logout) ", "Logout from server",
                        " exit) ", "Exit");
                opt_shown = TRUE;
                if (poll(&mypoll, 1, 5000)) {
                    /* read in command and message from stdio */
                    fgets(option, 2048, stdin);
                    /* set the length of substring containing \n to 0, hence discarding the newline char */
                    option[strcspn(option, "\n")] = 0;
                }
            } else {
                if (poll(&mypoll, 1, 500)) {
                    /* read in command and message from stdio */
                    fgets(option, 2048, stdin);
                    /* set the length of substring containing \n to 0, hence discarding the newline char */
                    option[strcspn(option, "\n")] = 0;
                }
            }

            /* get the command */
            token = strtok(option, delim);
            if (token) {
                strncpy(cmd, token, strlen(token));
            }

            int j = 0;
            while (token) {
                if (++j == 1) {
                    /* walk through the next token; friend's username */
                    token = strtok(NULL, delim);
                    if (token)
                        strncpy(friend_un, token, strlen(token));
                    else break;
                } else {
                    /* walk through the next token; message to friend */
                    token = strtok(NULL, delim);
                    if (token) {
                        strncat(msg, token, strlen(token));
                        strncat(msg, " ", strlen(" "));
                    } else break;
                }
            }

            pthread_t sniffer_thread;
            int *n;
            n = malloc(1);
            *n = 0;

            if (strcmp(cmd, "m") == 0) {

                if (pthread_create(&sniffer_thread, NULL, chat_initiator, (void*) n) < 0) {
                    perror("Could not create message handler");
                    return EXIT_FAILURE;
                }

                /* Now join the thread , so that we dont terminate before the thread */
                pthread_join(sniffer_thread, NULL);

            } else if (strcmp(cmd, "i") == 0) {

                list_shown = TRUE;
                if (pthread_create(&sniffer_thread, NULL, inviter, (void *) n) < 0) {
                    perror("Could not create inviter");
                    return EXIT_FAILURE;
                }

                pthread_join(sniffer_thread, NULL);

            } else if (strcmp(cmd, "ia") == 0) {

                list_shown = TRUE;
                if (pthread_create(&sniffer_thread, NULL, invite_acceptor, (void *) n) < 0) {
                    perror("Could not create invite acceptor");
                    return EXIT_FAILURE;
                }

                pthread_join(sniffer_thread, NULL);

            } else if (strcmp(cmd, "req") == 0) {

                list_shown = FALSE;
                if (pthread_create(&sniffer_thread, NULL, requester, (void *) n) < 0) {
                    perror("Could not create requester");
                    return EXIT_FAILURE;
                }

                pthread_join(sniffer_thread, NULL);

            } else if (strcmp(cmd, "logout") == 0) {

                opt_shown = FALSE;
                usage = USAGE_LOG_OUT;
                if (pthread_create(&sniffer_thread, NULL, server_consultant, (void *) n) < 0) {
                    perror("Could not create logout requester");
                    return EXIT_FAILURE;
                }

                pthread_join(sniffer_thread, NULL);

            } else if (strcmp(cmd, "exit") == 0) {

                opt_shown = FALSE;
                if (pthread_create(&sniffer_thread, NULL, terminator, (void *) n) < 0) {
                    perror("Could not create terminator");
                    return EXIT_FAILURE;
                }

                /* Now join the thread , so that we dont terminate before the thread */
                pthread_join(sniffer_thread, NULL);

                puts("\nGood bye!\n");
                return EXIT_SUCCESS;

                usage = USAGE_LOG_IN;
            } else if (strcmp(cmd, "") == 0 || strcmp(cmd, " ") == 0 || strcmp(cmd, "\t") == 0) {
            } else {
                puts("\nInvalid choice");
            }
        }
    }

    /* Now join the thread , so that we dont terminate before the thread */
    pthread_join(sniffer_thread, NULL);

    puts("\nGood bye!\n");

    /* close the socket for creating client to server connection */
    close(sock_d_to_create_s_conn);
    sock_created = FALSE;

}