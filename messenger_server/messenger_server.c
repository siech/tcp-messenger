/*
 * File:   messenger_server.c
 * Author: akhila vegesna
 *
 * Created on November 8, 2016, 10:29 AM
 */

/**
 * htons() host to network short
 * htonl() host to network long
 * ntohs() network to host short
 * ntohl() network to host long
 **/

#include<stdio.h>
#include<string.h>    /* strlen */
#include<stdlib.h>    /* strlen */
#include<sys/socket.h>
#include <sys/types.h>
#include<arpa/inet.h> /* inet_addr */
#include<unistd.h>    /* write */
/* for threading. Link with -pthread */
#include<pthread.h>
#include <netdb.h>
#include <rpc/types.h>
/* handles signal such as SIGINT */
#include <signal.h>
#include <ctype.h>

#define SIZE sizeof(int)

#define MAX_NUM 20

#define USAGE_REG 1
#define USAGE_UNR -1
#define USAGE_REQ 0
#define USAGE_LOG_IN 2
#define USAGE_LOG_OUT -2
#define USAGE_I -3
#define USAGE_IA 3
#define UN_AVL 200
#define UN_UNAVL 500
#define REG_SUC 25
#define REG_FAIL -25
#define LOG_SUC 26
#define LOG_FAIL -26

void *connection_handler(void *);

enum msg_type {
    MESSAGE, INVITE, INVITE_ACCEPT, CLIENTS, OTHER
};

struct clients {
    int p;
    char ip[20];
    char username[20];
    char password[30];
    int extra_inf;
    char friends_uns[10][20];
};

/**
 * Registration details
 */
struct reg_data {
    int port;
    int usage;
    char username[20];
    char password[30];
    char p_f_un[20];
    char msg[2048];
};

struct config {
    char keyword[20];
    char value[20];
};

struct message {
    char msg[2048];
    char sender_un[20];
    enum msg_type type;
};

struct config conf[5];
struct sockaddr_in client;
struct sockaddr_in this_server;
struct reg_data reg_inf[1];
struct message msg_to_client[1];

char conf_file_n[20];
char users_file_n[20];
char locations_file_n[] = "locations.txt";

int sock_d_to_create_c_conn = 0;

FILE *users_file;
FILE *locations_file;

void sigHandler(int dummy) {

    if (sock_d_to_create_c_conn) close(sock_d_to_create_c_conn);

    puts("\n\n  ****************************************************************\n"
            "\t\tSomeone ctrl c'd. Exiting gracefully\n"
            "  ****************************************************************\n");
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {

    // <editor-fold defaultstate="collapsed" desc="main logic">
    if (argc != 3) {
        puts("\nusage: messenger_server users_info_file configuration_file\n");
        return EXIT_FAILURE;
    }

    /* defer control to sigHandler to prevent ctrl+c default action */
    signal(SIGINT, sigHandler);

    FILE *conf_file;
    char temp_val[20];

    strncpy(conf_file_n, argv[2], strlen(argv[2]));
    strncpy(users_file_n, argv[1], strlen(argv[2]));

    conf_file = fopen(conf_file_n, "r");
    if (conf_file == NULL) {
        printf("Couldn't open %s for reading.\n", conf_file_n);
        perror("");
        exit(EXIT_FAILURE);
    }

    int repeat = 1;
    int i = 0;
    while (repeat) {
        if ((fscanf(conf_file, "%s\n", temp_val)) != 1) {
            repeat = 0;
            break;
        }

        const char delim[2] = ":";
        char *token;

        /* get the first token; keyword */
        token = strtok(temp_val, delim);

        /* server host is at index 0 */
        if (strcmp(token, "port") == 0)i = 0;
            /* server host is at index 1 */
        else if (strcmp(token, "servhost") == 0)i = 1;

        strncpy(conf[i].keyword, token, strlen(token));

        /* walk through the next token; value */
        token = strtok(NULL, delim);
        strncpy(conf[i].value, token, strlen(token));
    }

    fclose(conf_file);

    int server_socket_no, client_sock_no, c, *new_sock;
    struct sockaddr_in server;

    /* Then it will create a socket using the socket() system call. */
    server_socket_no = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_no == -1) {
        puts("Could not create socket");
        exit(EXIT_FAILURE);
    }

    /* Prepare the sockaddr_in structure */
    server.sin_family = AF_INET;
    server.sin_port = htons(atoi(conf[0].value));
    server.sin_addr.s_addr = INADDR_ANY;
    /* uncomment to use address from file */
    /* server.sin_addr.s_addr = inet_addr(conf[0].value); */

    /* It binds its address to the socket using the bind() system call */
    if (bind(server_socket_no, (struct sockaddr *) &server, sizeof (server)) < 0) {
        /* print the error message */
        perror("An error occurred: bind failed; ");
        return EXIT_FAILURE;
    }

    struct addrinfo hints, *info, *p;
    int gai_result;

    char hostname[1024];
    hostname[1023] = '\0';
    gethostname(hostname, 1023);

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET; /*IPV4*/
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_CANONNAME;

    if ((gai_result = getaddrinfo(hostname, "http", &hints, &info)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(gai_result));
        exit(1);
    }

    puts("\nServer is up and listening...");

    for (p = info; p != NULL; p = p->ai_next) {
        /* The server machine's fully-qualified domain name */
        printf("%-12s %s\n", "Host name:", p->ai_canonname);
    }

    /* and the (assigned) port number are displayed on the screen. */
    printf("%-12s %s\n", "IP address:", inet_ntoa(server.sin_addr));
    printf("%-12s %d\n\n", "Port number:", ntohs(server.sin_port));

    /* free the memory occupied by addrinfo */
    freeaddrinfo(info);

    /* create locations and users files */
    locations_file = fopen("locations.txt", "w");
    fclose(locations_file);
    users_file = fopen(users_file_n, "a");
    fclose(users_file);

    /* Then listen() call is used to indicate that the server is now ready to receive connect requests from clients. */
    listen(server_socket_no, 3);

    while ((client_sock_no = accept(server_socket_no, (struct sockaddr *) &client, (socklen_t*) & c))) {

        pthread_t sniffer_thread;
        new_sock = malloc(1);
        *new_sock = client_sock_no;

        puts("... New connection accepted");

        puts("\nCLIENT DETAILS");
        puts("````````````");
        printf("\tIPaddress \t\t: %s\n", inet_ntoa(client.sin_addr));
        /* printf("\tClient port is\t\t: %d\n", (int) ntohs(client.sin_port)); */

        if (pthread_create(&sniffer_thread, NULL, connection_handler, (void*) new_sock) < 0) {
            perror("Could not create connection handler");
            return EXIT_FAILURE;
        }

        /* Now join the thread , so that we dont terminate before the thread ends */
        pthread_join(sniffer_thread, NULL);

    }

    if (client_sock_no < 0) {
        perror("accept failed");

        return EXIT_FAILURE;
    }

    shutdown(client_sock_no, 2);
    close(client_sock_no);

    return EXIT_SUCCESS;
    // </editor-fold>

}

/* This will handle connection for each client */
void *connection_handler(void *socket_desc) {
    /* Get the socket descriptor */
    int client_sock_no = *(int*) socket_desc;
    int buff[1];
    memset(buff, 0, sizeof (buff));
    struct clients registered[10];
    memset(registered, 0, sizeof (registered));
    memset(reg_inf, 0, sizeof (struct reg_data));
    FILE *users_file;

    if (recv(client_sock_no, reg_inf, sizeof (struct reg_data), 0) > 0) {
        printf("\tCommuncation port\t: %d\n", reg_inf[0].port);
        printf("\tUsername\t\t: %s\n", reg_inf[0].username);
        printf("\tPassword\t\t: %s\n\n", reg_inf[0].password);

    }

    int total_online_users = 0;
    switch (reg_inf[0].usage) {
        case USAGE_LOG_IN:

            // <editor-fold defaultstate="collapsed" desc="login logic">
            puts("Client seeking to login");

            /* login success */
            int log_s = FALSE;

            /* if provided address is valid; not 0.0.0.0 */
            if (strcmp(inet_ntoa(client.sin_addr), "0.0.0.0") != 0) {

                /* open the file for reading */
                users_file = fopen(users_file_n, "r");
                if (users_file == NULL) {
                    perror("Couldn't open users_file_n for reading.\n");
                    exit(EXIT_FAILURE);
                }

                char read_l[1024];
                int repeat = 1;
                char temp_val[20];

                while (repeat) {

                    memset(read_l, 0, sizeof (read_l));
                    if ((fscanf(users_file, "%s\n", read_l)) != 1) {
                        repeat = 0;
                        break;
                    }

                    const char delim[2] = "|";
                    char *token;

                    /* get and check the username */
                    token = strtok(read_l, delim);
                    memset(temp_val, 0, sizeof (temp_val));
                    strncpy(temp_val, token, strlen(token));
                    if ((strcmp(temp_val, reg_inf[0].username)) == 0) {

                        /* walk through the next token; password */
                        token = strtok(NULL, delim);
                        memset(temp_val, 0, sizeof (temp_val));
                        strncpy(temp_val, token, strlen(token));
                        if ((strcmp(temp_val, reg_inf[0].password)) == 0) {
                            /* login credentials valid */
                            log_s = TRUE;
                            puts("\t..Login credentials valid");
                            break;
                        }
                    }
                }

                /* username not available. abort */
                if (!log_s) {

                    /* clear the content in registered data structure */
                    memset(registered, 0, sizeof (registered));

                    /* inform client that username is unavailable */
                    registered[0].extra_inf = LOG_FAIL;
                    puts("\t..Login failed; invalid login credentials");

                    /* send the information */
                    write(client_sock_no, registered, sizeof (registered));

                    break;
                }

                /* open users and locations files */
                locations_file = fopen(locations_file_n, "r");
                if (locations_file == NULL) {
                    perror("Couldn't open file for reading.\n");
                    exit(EXIT_FAILURE);
                }

                int j = 0;
                char read_l_l[256];
                char *token;
                const char delim[2] = "|";

                memset(registered, 0, sizeof (registered));

                for (;;) {
                    if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                        break;
                    }

                    /* get the username */
                    token = strtok(read_l_l, delim);
                    if ((strcmp(reg_inf[0].username, token)) != 0) {
                        /* write the username */
                        strncpy(registered[j].username, token, strlen(token));

                        /* walk through the next token; ip address */
                        token = strtok(NULL, delim);
                        strncpy(registered[j].ip, token, strlen(token));

                        /* walk through the next token; port number */
                        memset(token, 0, sizeof (token));
                        registered[j].p = atoi(strtok(NULL, delim));
                        memset(token, 0, sizeof (token));
                        j++;
                    }
                }

                fclose(locations_file);

                /* open client registration file */
                locations_file = fopen(locations_file_n, "w");
                if (locations_file == NULL) {
                    perror("Couldn't open locations_file_n for writing.\n");
                    exit(EXIT_FAILURE);
                }

                /* register clients in file */
                int i = 0;
                total_online_users = 0;
                while (i < j) {
                    fprintf(locations_file, "%s|", registered[i].username);
                    fprintf(locations_file, "%s|", registered[i].ip);
                    fprintf(locations_file, "%d\n", registered[i].p);
                    i++, total_online_users++;
                }

                /* write client's location to file */
                fprintf(locations_file, "%s|", reg_inf[0].username);
                fprintf(locations_file, "%s|", inet_ntoa(client.sin_addr));
                fprintf(locations_file, "%d\n", (int) reg_inf[0].port);
                total_online_users++;

                /* close the files */
                fclose(locations_file);
            } else {
                /* clear the content in registered data structure */
                memset(registered, 0, sizeof (registered));

                /* inform client that username is unavailable */
                registered[0].extra_inf = LOG_FAIL;
                puts("\t..Login failed; invalid client address\n");

                /* send the information */
                write(client_sock_no, registered, sizeof (registered));
                break;
            }

            /* username not available. inform client */
            if (!log_s) {
                /* clear the content in registered data structure */
                memset(registered, 0, sizeof (registered));

                /* inform client that username is unavailable */
                registered[0].extra_inf = LOG_FAIL;
                puts("\t..Login failed; invalid login credentials");

                /* send the information */
                write(client_sock_no, registered, sizeof (registered));
                break;
            }

            char read_u_l[1024];
            char read_l_l[256];

            // <editor-fold defaultstate="collapsed" desc="read the list of users">

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* open the locations file for reading */
            locations_file = fopen(locations_file_n, "r");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* clear the content in registered data structure */
            memset(registered, 0, sizeof (registered));
            memset(read_u_l, 0, sizeof (read_u_l));
            memset(read_l_l, 0, sizeof (read_l_l));

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* create the users information */
            int j = 0;
            for (;;) {
                if ((fscanf(users_file, "%s\n", read_u_l)) != 1) {
                    break;
                }

                const char delim[2] = "|";
                char *token;

                /* get the username */
                token = strtok(read_u_l, delim);
                strncpy(registered[j].username, token, strlen(token));

                /* get the password */
                token = strtok(NULL, delim);
                strncpy(registered[j].password, token, strlen(token));

                char f_l[50];
                char *friends;

                /* delete the memory holding friends list */
                memset(registered[j].friends_uns, 0, sizeof (registered[j].friends_uns));

                /* get the friends */
                friends = strtok(NULL, delim);
                if (friends) {
                    strncpy(f_l, friends, strlen(friends));

                    const char del[2] = ";";
                    char *tok;
                    int k = 0;
                    tok = strtok(f_l, del);
                    while (tok) {

                        /* get each friend */
                        int w = 0, f_exists = FALSE;
                        if (strcmp(registered[j].username, tok) != 0) {
                            for (w = 0; w < 10; w++) {
                                if (strcmp(registered[j].friends_uns[w], "") == 0) break;
                                else if (strcmp(registered[j].friends_uns[w], tok) == 0) {
                                    f_exists = TRUE;
                                    break;
                                }
                            }
                        } else f_exists = TRUE;

                        if (!f_exists) strncpy(registered[j].friends_uns[k], tok, strlen(tok));

                        k++;

                        tok = strtok(NULL, del);
                    }
                }

                for (;;) {
                    if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                        break;
                    }

                    /* get the username */
                    token = strtok(read_l_l, delim);
                    if ((strcmp(registered[j].username, token)) == 0) {

                        /* walk through the next token; ip address */
                        token = strtok(NULL, delim);
                        strncpy(registered[j].ip, token, strlen(token));

                        /* walk through the next token; port number */
                        memset(token, 0, sizeof (token));
                        registered[j].p = atoi(strtok(NULL, delim));
                        memset(token, 0, sizeof (token));
                        break;
                    }
                }

                /* clear contents in variables */
                memset(read_u_l, 0, sizeof (read_u_l));
                memset(read_l_l, 0, sizeof (read_l_l));
                j++;
            }

            /* close users and locations file and send users information to client */
            fclose(users_file);
            fclose(locations_file);
            write(client_sock_no, registered, sizeof (registered));

            // </editor-fold>

            puts("... Client logged in");

            // <editor-fold defaultstate="collapsed" desc="create conn to clients and send list of online users ">
            int i = 0;
            for (;;) {
                if (strcmp(registered[i].username, reg_inf[0].username) == 0) {
                    i++;
                    continue;
                }
                if (registered[i].p == 0) {
                    break;
                }

                /* Sock used in creating conn to client */
                sock_d_to_create_c_conn = socket(AF_INET, SOCK_STREAM, 0);
                if (sock_d_to_create_c_conn == -1) {
                    printf("\nCould not create socket");
                }

                /* Initialize random number generator */
                srand((unsigned) time(NULL));

                /* Generate a random port number */
                int port = rand() % 7000 + 70000;

                /* Details of socket to create conn to client */
                memset(&this_server, 0, sizeof (struct sockaddr_in));
                this_server.sin_family = AF_INET;
                this_server.sin_port = htons(port);
                this_server.sin_addr.s_addr = INADDR_ANY;

                /* Details for client socket */
                memset(&client, 0, sizeof (struct sockaddr_in));
                client.sin_family = AF_INET;
                client.sin_port = htons(registered[i].p);
                client.sin_addr.s_addr = inet_addr(registered[i].ip);

                /* Connect to the client */
                if (connect(sock_d_to_create_c_conn, (struct sockaddr *) &client, sizeof (client)) < 0) {
                    perror("connect failed. Error");
                    exit(EXIT_FAILURE);
                }

                /* prepare the invite message */
                msg_to_client[0].type = CLIENTS;

                /* send the invite message to client */
                write(sock_d_to_create_c_conn, msg_to_client, sizeof (msg_to_client));

                /* send list of online users */
                write(sock_d_to_create_c_conn, registered, sizeof (registered));

                /* close connection to client */
                close(sock_d_to_create_c_conn);
                i++;
            }
            // </editor-fold>

            registered[0].extra_inf = LOG_SUC;
            write(client_sock_no, registered, sizeof (registered));

            puts("... Handler assigned\n");
            printf("... Total number of users online: %d\n\n", total_online_users);
            break;

            // </editor-fold>

        case USAGE_REG:

            // <editor-fold defaultstate="collapsed" desc="register logic">
            puts("Client seeking to register its details");

            /* username available */
            int un_avl = TRUE;

            /* if provided address is valid; not 0.0.0.0 */
            if (strcmp(inet_ntoa(client.sin_addr), "0.0.0.0") != 0) {

                /* open the file for reading */
                users_file = fopen(users_file_n, "r");
                if (users_file == NULL) {
                    perror("Couldn't open users_file_n for reading.\n");
                    exit(EXIT_FAILURE);
                }

                char read_l[1024];
                int repeat = 1;
                char temp_val[20];

                while (repeat) {

                    memset(read_l, 0, sizeof (read_l));
                    if ((fscanf(users_file, "%s\n", read_l)) != 1) {
                        repeat = 0;
                        break;
                    }

                    const char s[2] = "|";
                    char *token;

                    /* get the first token */
                    token = strtok(read_l, s);
                    memset(temp_val, 0, sizeof (temp_val));
                    strncpy(temp_val, token, strlen(token));
                    if ((strcmp(temp_val, reg_inf[0].username)) == 0) {
                        puts("\t...Username not available");
                        un_avl = FALSE;
                        break;
                    }
                }

                /* username not available. abort */
                if (!un_avl) {
                    /* clear the content in registered data structure */
                    memset(registered, 0, sizeof (registered));

                    /* inform client that username is unavailable */
                    registered[0].extra_inf = UN_UNAVL;
                    puts("\t..Registration failed; username unavailable");

                    /* send the information */
                    write(client_sock_no, registered, sizeof (registered));

                    break;
                }

                /* open users file */
                users_file = fopen(users_file_n, "a+");
                locations_file = fopen(locations_file_n, "r");
                if (users_file == NULL || locations_file == NULL) {
                    perror("Couldn't open file for writing.\n");
                    exit(EXIT_FAILURE);
                }

                /* register client
                   ``````````````` */
                /* write to users file */
                fprintf(users_file, "%s|", reg_inf[0].username);
                fprintf(users_file, "%s|\n", reg_inf[0].password);

                /* close the file */
                fclose(users_file);

                int j = 0;
                char read_l_l[256];
                char *token;
                const char delim[2] = "|";

                memset(registered, 0, sizeof (registered));

                for (;;) {
                    if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                        break;
                    }

                    /* get the username */
                    token = strtok(read_l_l, delim);
                    if ((strcmp(reg_inf[0].username, token)) != 0) {
                        /* write the username */
                        strncpy(registered[j].username, token, strlen(token));

                        /* walk through the next token; ip address */
                        token = strtok(NULL, delim);
                        strncpy(registered[j].ip, token, strlen(token));

                        /* walk through the next token; port number */
                        memset(token, 0, sizeof (token));
                        registered[j].p = atoi(strtok(NULL, delim));
                        memset(token, 0, sizeof (token));
                        j++;
                    }
                }

                fclose(locations_file);

                /* open client registration file */
                locations_file = fopen(locations_file_n, "w");
                if (locations_file == NULL) {
                    perror("Couldn't open locations_file_n for writing.\n");
                    exit(EXIT_FAILURE);
                }

                /* update locations in file */
                int i = 0;
                total_online_users = 0;
                while (i < j) {
                    fprintf(locations_file, "%s|", registered[i].username);
                    fprintf(locations_file, "%s|", registered[i].ip);
                    fprintf(locations_file, "%d\n", registered[i].p);
                    i++, total_online_users++;
                }

                /* write client's location to file */
                fprintf(locations_file, "%s|", reg_inf[0].username);
                fprintf(locations_file, "%s|", inet_ntoa(client.sin_addr));
                fprintf(locations_file, "%d\n", (int) reg_inf[0].port);
                total_online_users++;

                /* close the files */
                fclose(locations_file);

            } else {
                /* clear the content in registered data structure */
                memset(registered, 0, sizeof (registered));

                /* inform client that username is unavailable */
                registered[0].extra_inf = REG_FAIL;
                puts("\t..Registration failed; invalid client address\n");

                /* send the information */
                write(client_sock_no, registered, sizeof (registered));
                break;
            }

            /* username not available. inform client */
            if (!un_avl) {
                /* clear the content in registered data structure */
                memset(registered, 0, sizeof (registered));

                /* inform client that username is unavailable */
                registered[0].extra_inf = UN_UNAVL;
                puts("\t..Registration failed; username unavailable");

                /* send the information */
                write(client_sock_no, registered, sizeof (registered));
                break;
            }

            // <editor-fold defaultstate="collapsed" desc="create the list of users">

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* open the locations file for reading */
            locations_file = fopen(locations_file_n, "r");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* clear the content in registered data structure */
            memset(registered, 0, sizeof (registered));
            memset(read_u_l, 0, sizeof (read_u_l));
            memset(read_l_l, 0, sizeof (read_l_l));

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* create the users information */
            j = 0;
            for (;;) {
                if ((fscanf(users_file, "%s\n", read_u_l)) != 1) {
                    break;
                }

                const char delim[2] = "|";
                char *token;

                /* get the username */
                token = strtok(read_u_l, delim);
                strncpy(registered[j].username, token, strlen(token));

                /* get the password */
                token = strtok(NULL, delim);
                strncpy(registered[j].password, token, strlen(token));

                char f_l[50];
                char *friends;

                /* delete the memory holding friends list */
                memset(registered[j].friends_uns, 0, sizeof (registered[j].friends_uns));

                /* get the friends */
                friends = strtok(NULL, delim);
                if (friends) {
                    strncpy(f_l, friends, strlen(friends));

                    const char del[2] = ";";
                    char *tok;
                    int k = 0;
                    tok = strtok(f_l, del);
                    while (tok) {

                        /* get each friend */
                        int w = 0, f_exists = FALSE;
                        if (strcmp(registered[i].username, tok) != 0) {
                            for (w = 0; w < 10; w++) {
                                if (strcmp(registered[j].friends_uns[w], "") == 0) break;
                                else if (strcmp(registered[j].friends_uns[w], tok) == 0) {
                                    f_exists = TRUE;
                                    break;
                                }
                            }
                        } else f_exists = TRUE;

                        if (!f_exists) strncpy(registered[j].friends_uns[k], tok, strlen(tok));

                        k++;

                        tok = strtok(NULL, del);
                    }
                }

                for (;;) {
                    if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                        break;
                    }

                    /* get the username */
                    token = strtok(read_l_l, delim);
                    if ((strcmp(registered[j].username, token)) == 0) {

                        /* walk through the next token; ip address */
                        token = strtok(NULL, delim);
                        strncpy(registered[j].ip, token, strlen(token));

                        /* walk through the next token; port number */
                        memset(token, 0, sizeof (token));
                        registered[j].p = atoi(strtok(NULL, delim));
                        memset(token, 0, sizeof (token));
                        break;
                    }
                }

                /* clear contents in variables */
                memset(read_u_l, 0, sizeof (read_u_l));
                memset(read_l_l, 0, sizeof (read_l_l));
                j++;
            }

            /* close users and locations file and send users information to client */
            fclose(users_file);
            fclose(locations_file);
            write(client_sock_no, registered, sizeof (registered));

            // </editor-fold>

            puts("... Client registered");

            // <editor-fold defaultstate="collapsed" desc="create conn to clients and send list of online users ">
            i = 0;
            for (i = 0; i < 10; i++) {
                if (strcmp(registered[i].username, reg_inf[0].username) == 0) {
                    continue;
                }
                if (registered[i].p == 0) {
                    continue;
                }

                /* Sock used in creating conn to client */
                sock_d_to_create_c_conn = socket(AF_INET, SOCK_STREAM, 0);
                if (sock_d_to_create_c_conn == -1) {
                    printf("\nCould not create socket");
                }

                /* Initialize random number generator */
                srand((unsigned) time(NULL));

                /* Generate a random port number */
                int port = rand() % 7000 + 70000;

                /* Details of socket to create conn to client */
                memset(&this_server, 0, sizeof (struct sockaddr_in));
                this_server.sin_family = AF_INET;
                this_server.sin_port = htons(port);
                this_server.sin_addr.s_addr = INADDR_ANY;

                /* Details for client socket */
                memset(&client, 0, sizeof (struct sockaddr_in));
                client.sin_family = AF_INET;
                client.sin_port = htons(registered[i].p);
                client.sin_addr.s_addr = inet_addr(registered[i].ip);

                /* Connect to the client */
                if (connect(sock_d_to_create_c_conn, (struct sockaddr *) &client, sizeof (client)) < 0) {
                    perror("connect failed. Error");
                    exit(EXIT_FAILURE);
                }

                /* prepare the invite message */
                msg_to_client[0].type = CLIENTS;

                /* send the invite message to client */
                write(sock_d_to_create_c_conn, msg_to_client, sizeof (msg_to_client));

                /* send list of online users */
                write(sock_d_to_create_c_conn, registered, sizeof (registered));

                /* close connection to client */
                close(sock_d_to_create_c_conn);
            }
            // </editor-fold>

            registered[0].extra_inf = REG_SUC;
            write(client_sock_no, registered, sizeof (registered));

            puts("... Handler assigned\n");
            printf("... Total number of users online: %d\n\n", total_online_users);
            break;
            // </editor-fold>

        case USAGE_I:

            // <editor-fold defaultstate="collapsed" desc="invite logic">
            puts("Client requesting to invite a client");

            // <editor-fold defaultstate="collapsed" desc="create conn to client and inform of client request">
            /* Sock used in creating conn to client */
            sock_d_to_create_c_conn = socket(AF_INET, SOCK_STREAM, 0);
            if (sock_d_to_create_c_conn == -1) {
                printf("\nCould not create socket");
            }

            /* Initialize random number generator */
            srand((unsigned) time(NULL));

            /* Generate a random port number */
            int port = rand() % 7000 + 70000;

            /* Details of socket to create conn to client */
            memset(&this_server, 0, sizeof (struct sockaddr_in));
            this_server.sin_family = AF_INET;
            this_server.sin_port = htons(port);
            this_server.sin_addr.s_addr = INADDR_ANY;

            /* clear the content in registered data structure */
            memset(registered, 0, sizeof (registered));
            memset(read_u_l, 0, sizeof (read_u_l));
            memset(read_l_l, 0, sizeof (read_l_l));

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* create the users information */
            j = 0;
            for (;;) {
                if ((fscanf(users_file, "%s\n", read_u_l)) != 1) {
                    break;
                }

                const char delim[2] = "|";
                char *token;

                /* get the username */
                token = strtok(read_u_l, delim);
                strncpy(registered[j].username, token, strlen(token));

                for (;;) {
                    if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                        break;
                    }

                    /* get the username */
                    token = strtok(read_l_l, delim);
                    if ((strcmp(registered[j].username, token)) == 0) {

                        /* walk through the next token; ip address */
                        token = strtok(NULL, delim);
                        strncpy(registered[j].ip, token, strlen(token));

                        /* walk through the next token; port number */
                        memset(token, 0, sizeof (token));
                        registered[j].p = atoi(strtok(NULL, delim));
                        memset(token, 0, sizeof (token));
                        break;
                    }
                }

                /* clear content in variables */
                memset(read_u_l, 0, sizeof (read_u_l));
                memset(read_l_l, 0, sizeof (read_l_l));
                j++;
            }

            /* close users and locations file */
            fclose(users_file);

            memset(read_l_l, 0, sizeof (read_l_l));

            /* open client registration file */
            locations_file = fopen(locations_file_n, "r");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            char *token;
            char client_ip[20];
            const char delim[2] = "|";
            int client_p;
            j = 0;

            for (;;) {
                if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                    break;
                }

                /* check the username for match */
                token = strtok(read_l_l, delim);
                if ((strcmp(reg_inf[0].p_f_un, token)) == 0) {
                    /* walk through the next token; ip address */
                    token = strtok(NULL, delim);
                    strncpy(client_ip, token, strlen(token));

                    /* walk through the next token; port number */
                    memset(token, 0, sizeof (token));
                    client_p = atoi(strtok(NULL, delim));
                    memset(token, 0, sizeof (token));
                    break;
                }
            }

            fclose(locations_file);

            /* Details for client socket */
            memset(&client, 0, sizeof (struct sockaddr_in));
            client.sin_family = AF_INET;
            client.sin_port = htons(client_p);
            client.sin_addr.s_addr = inet_addr(client_ip);

            /* Connect to the client */
            if (connect(sock_d_to_create_c_conn, (struct sockaddr *) &client, sizeof (client)) < 0) {
                perror("connect failed. Error");
                exit(EXIT_FAILURE);
            } else {
                /* puts("\nConnected to client"); */
            }

            /* prepare the invite message */
            msg_to_client[0].type = INVITE;
            strncpy(msg_to_client[0].msg, reg_inf[0].msg, sizeof reg_inf[0].msg);
            strncpy(msg_to_client[0].sender_un, reg_inf[0].username, sizeof reg_inf[0].username);

            /* send the invite message to client */
            write(sock_d_to_create_c_conn, msg_to_client, sizeof (msg_to_client));

            /* close connection to client */
            close(sock_d_to_create_c_conn);

            // </editor-fold>

            printf("... Invitation sent to %s\n\n", reg_inf[0].p_f_un);
            break;
            // </editor-fold>

        case USAGE_IA:

            // <editor-fold defaultstate="collapsed" desc="invite accept logic">
            puts("Client requesting to accept invite");

            // <editor-fold defaultstate="collapsed" desc="create conn to client and inform of invite acceptance">

            /* Sock used in creating conn to client */
            sock_d_to_create_c_conn = socket(AF_INET, SOCK_STREAM, 0);
            if (sock_d_to_create_c_conn == -1) {
                printf("\nCould not create socket");
            }

            /* Initialize random number generator */
            srand((unsigned) time(NULL));

            /* Generate a random port number */
            port = rand() % 7000 + 70000;

            /* Details of socket to create conn to client */
            memset(&this_server, 0, sizeof (struct sockaddr_in));
            this_server.sin_family = AF_INET;
            this_server.sin_port = htons(port);
            this_server.sin_addr.s_addr = INADDR_ANY;

            /* clear the content in registered data structure */
            memset(registered, 0, sizeof (registered));
            memset(read_u_l, 0, sizeof (read_u_l));

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* create the users information */
            j = 0;
            for (;;) {
                if ((fscanf(users_file, "%s\n", read_u_l)) != 1) {
                    break;
                }

                const char delim[2] = "|";
                char *token;

                /* get the username */
                token = strtok(read_u_l, delim);
                strncpy(registered[j].username, token, strlen(token));

                /* get the password */
                token = strtok(NULL, delim);
                strncpy(registered[j].password, token, strlen(token));

                char f_l[50];
                char *friends;

                /* delete the memory holding friends list */
                memset(registered[j].friends_uns, 0, sizeof (registered[j].friends_uns));

                /* get the friends */
                friends = strtok(NULL, delim);
                if (friends) {
                    strncpy(f_l, friends, strlen(friends));

                    const char del[2] = ";";
                    char *tok;
                    int k = 0;
                    tok = strtok(f_l, del);
                    while (tok) {

                        /* get each friend */
                        int w = 0, f_exists = FALSE;
                        if (strcmp(registered[i].username, tok) != 0) {
                            for (w = 0; w < 10; w++) {
                                if (strcmp(registered[j].friends_uns[w], "") == 0) break;
                                else if (strcmp(registered[j].friends_uns[w], tok) == 0) {
                                    f_exists = TRUE;
                                    break;
                                }
                            }
                        } else f_exists = TRUE;

                        if (!f_exists) strncpy(registered[j].friends_uns[k], tok, strlen(tok));

                        k++;

                        tok = strtok(NULL, del);
                    }
                }

                /* clear content in read line buffer */
                memset(read_u_l, 0, sizeof (read_u_l));
                j++;
            }

            /* close users and locations file */
            fclose(users_file);

            /* open client registration file */
            users_file = fopen(users_file_n, "w");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for writing.\n");
                exit(EXIT_FAILURE);
            }

            /* update clients in file */
            i = 0;
            int k;
            while (i < j) {
                if (i > 0) fprintf(users_file, "\n%s|", registered[i].username);
                else fprintf(users_file, "%s|", registered[i].username);
                fprintf(users_file, "%s|", registered[i].password);
                for (k = 0; k < 10; k++) {
                    if (strcmp(registered[i].friends_uns[k], "") != 0 && !isdigit(registered[i].friends_uns[k][0])) {

                        int w = 0, f_exists = FALSE;
                        if (strcmp(registered[i].username, registered[i].friends_uns[k]) != 0) {
                            for (w = 0; w < 10; w++) {
                                if (strcmp(registered[j].friends_uns[w], "") == 0) break;
                                else if (strcmp(registered[j].friends_uns[w], registered[i].friends_uns[k]) == 0) {
                                    f_exists = TRUE;
                                    break;
                                }
                            }
                        } else f_exists = TRUE;

                        if (!f_exists) fprintf(users_file, "%s;", registered[i].friends_uns[k]);

                    } else break;
                }

                /* update contact/friend list */
                if (strcmp(registered[i].username, reg_inf[0].p_f_un) == 0) {
                    fprintf(users_file, "%s;", reg_inf[0].username);
                }
                if (strcmp(registered[i].username, reg_inf[0].username) == 0) {
                    fprintf(users_file, "%s;", reg_inf[0].p_f_un);
                }
                i++;
            }

            fprintf(users_file, "\n");

            /* close the file */
            fclose(users_file);

            /* open client registration file */
            locations_file = fopen(locations_file_n, "r");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            memset(read_l_l, 0, sizeof (read_l_l));
            memset(client_ip, 0, sizeof (client_ip));
            const char lim[2] = "|";
            j = 0;

            for (;;) {
                if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                    break;
                }

                /* check the username for match */
                token = strtok(read_l_l, lim);
                if ((strcmp(reg_inf[0].p_f_un, token)) == 0) {
                    /* walk through the next token; ip address */
                    token = strtok(NULL, lim);
                    strncpy(client_ip, token, strlen(token));

                    /* walk through the next token; port number */
                    memset(token, 0, sizeof (token));
                    client_p = atoi(strtok(NULL, lim));
                    memset(token, 0, sizeof (token));
                    break;
                }
            }

            fclose(locations_file);

            /* Details for client socket */
            memset(&client, 0, sizeof (struct sockaddr_in));
            client.sin_family = AF_INET;
            client.sin_port = htons(client_p);
            client.sin_addr.s_addr = inet_addr(client_ip);

            /* Connect to the client */
            if (connect(sock_d_to_create_c_conn, (struct sockaddr *) &client, sizeof (client)) < 0) {
                perror("connect failed. Error");
                exit(EXIT_FAILURE);
            } else {
                /* puts("\nConnected to client"); */
            }

            /* prepare the invite message */
            msg_to_client[0].type = INVITE_ACCEPT;
            strncpy(msg_to_client[0].msg, reg_inf[0].msg, sizeof reg_inf[0].msg);
            strncpy(msg_to_client[0].sender_un, reg_inf[0].username, sizeof reg_inf[0].username);

            /* send the invite message to client */
            write(sock_d_to_create_c_conn, msg_to_client, sizeof (msg_to_client));

            /* close connection to client */
            close(sock_d_to_create_c_conn);

            // </editor-fold>

            printf("... Invitation acceptance sent to %s\n\n", reg_inf[0].p_f_un);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="read the list of users">

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* open the locations file for reading */
            locations_file = fopen(locations_file_n, "r");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* clear the content in registered data structure */
            memset(registered, 0, sizeof (registered));
            memset(read_u_l, 0, sizeof (read_u_l));
            memset(read_l_l, 0, sizeof (read_l_l));

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* create the users information */
            j = 0;
            for (;;) {
                if ((fscanf(users_file, "%s\n", read_u_l)) != 1) {
                    break;
                }

                const char delim[2] = "|";
                char *token;

                /* get the username */
                token = strtok(read_u_l, delim);
                strncpy(registered[j].username, token, strlen(token));

                /* get the password */
                token = strtok(NULL, delim);
                strncpy(registered[j].password, token, strlen(token));

                char f_l[50];
                char *friends;

                /* delete the memory holding friends list */
                memset(registered[j].friends_uns, 0, sizeof (registered[j].friends_uns));

                /* get the friends */
                friends = strtok(NULL, delim);
                if (friends) {
                    strncpy(f_l, friends, strlen(friends));

                    const char del[2] = ";";
                    char *tok;
                    int k = 0;
                    tok = strtok(f_l, del);
                    while (tok) {

                        /* get each friend */
                        int w = 0, f_exists = FALSE;
                        if (strcmp(registered[i].username, tok) != 0) {
                            for (w = 0; w < 10; w++) {
                                if (strcmp(registered[j].friends_uns[w], "") == 0) break;
                                else if (strcmp(registered[j].friends_uns[w], tok) == 0) {
                                    f_exists = TRUE;
                                    break;
                                }
                            }
                        } else f_exists = TRUE;

                        if (!f_exists) strncpy(registered[j].friends_uns[k], tok, strlen(tok));

                        k++;

                        tok = strtok(NULL, del);
                    }
                }

                for (;;) {
                    if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                        break;
                    }

                    /* get the username */
                    token = strtok(read_l_l, delim);
                    if ((strcmp(registered[j].username, token)) == 0) {

                        /* walk through the next token; ip address */
                        token = strtok(NULL, delim);
                        strncpy(registered[j].ip, token, strlen(token));

                        /* walk through the next token; port number */
                        memset(token, 0, sizeof (token));
                        registered[j].p = atoi(strtok(NULL, delim));
                        memset(token, 0, sizeof (token));
                        break;
                    }
                }

                /* clear contents in variables */
                memset(read_u_l, 0, sizeof (read_u_l));
                memset(read_l_l, 0, sizeof (read_l_l));
                j++;
            }

            /* close users and locations file and send users information to client */
            fclose(users_file);
            fclose(locations_file);
            write(client_sock_no, registered, sizeof (registered));

            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="create conn to clients and send list of online users ">
            i = 0;
            for (;;) {
                if (registered[i].p == 0) {
                    break;
                }

                /* Sock used in creating conn to client */
                sock_d_to_create_c_conn = socket(AF_INET, SOCK_STREAM, 0);
                if (sock_d_to_create_c_conn == -1) {
                    printf("\nCould not create socket");
                }

                /* Initialize random number generator */
                srand((unsigned) time(NULL));

                /* Generate a random port number */
                int port = rand() % 7000 + 70000;

                /* Details of socket to create conn to client */
                memset(&this_server, 0, sizeof (struct sockaddr_in));
                this_server.sin_family = AF_INET;
                this_server.sin_port = htons(port);
                this_server.sin_addr.s_addr = INADDR_ANY;

                /* Details for client socket */
                memset(&client, 0, sizeof (struct sockaddr_in));
                client.sin_family = AF_INET;
                client.sin_port = htons(registered[i].p);
                client.sin_addr.s_addr = inet_addr(registered[i].ip);

                /* Connect to the client */
                if (connect(sock_d_to_create_c_conn, (struct sockaddr *) &client, sizeof (client)) < 0) {
                    perror("connect failed. Error");
                    exit(EXIT_FAILURE);
                }

                /* send the invite message to client */
                msg_to_client[0].type = OTHER;
                write(sock_d_to_create_c_conn, msg_to_client, sizeof (msg_to_client));

                /* send list of online users */
                write(sock_d_to_create_c_conn, registered, sizeof (registered));

                /* close connection to client */
                close(sock_d_to_create_c_conn);
                i++;
            }
            // </editor-fold>
            break;

        case USAGE_REQ:

            // <editor-fold defaultstate="collapsed" desc="request logic">
            puts("Client requesting for list of peers");

            // <editor-fold defaultstate="collapsed" desc="read the list of users">

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* open the locations file for reading */
            locations_file = fopen(locations_file_n, "r");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* clear the content in registered data structure */
            memset(registered, 0, sizeof (registered));
            memset(read_u_l, 0, sizeof (read_u_l));
            memset(read_l_l, 0, sizeof (read_l_l));

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* create the users information */
            j = 0;
            for (;;) {
                if ((fscanf(users_file, "%s\n", read_u_l)) != 1) {
                    break;
                }

                const char delim[2] = "|";
                char *token;

                /* get the username */
                token = strtok(read_u_l, delim);
                strncpy(registered[j].username, token, strlen(token));

                /* get the password */
                token = strtok(NULL, delim);
                strncpy(registered[j].password, token, strlen(token));

                char f_l[50];
                char *friends;

                /* delete the memory holding friends list */
                memset(registered[j].friends_uns, 0, sizeof (registered[j].friends_uns));

                /* get the friends */
                friends = strtok(NULL, delim);
                if (friends) {
                    strncpy(f_l, friends, strlen(friends));

                    const char del[2] = ";";
                    char *tok;
                    int k = 0;
                    tok = strtok(f_l, del);
                    while (tok) {

                        /* get each friend */
                        int w = 0, f_exists = FALSE;
                        if (strcmp(registered[i].username, tok) != 0) {
                            for (w = 0; w < 10; w++) {
                                if (strcmp(registered[j].friends_uns[w], "") == 0) break;
                                else if (strcmp(registered[j].friends_uns[w], tok) == 0) {
                                    f_exists = TRUE;
                                    break;
                                }
                            }
                        } else f_exists = TRUE;

                        if (!f_exists) strncpy(registered[j].friends_uns[k], tok, strlen(tok));

                        k++;

                        tok = strtok(NULL, del);
                    }
                }

                for (;;) {
                    if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                        break;
                    }

                    /* get the username */
                    token = strtok(read_l_l, delim);
                    if ((strcmp(registered[j].username, token)) == 0) {

                        /* walk through the next token; ip address */
                        token = strtok(NULL, delim);
                        strncpy(registered[j].ip, token, strlen(token));

                        /* walk through the next token; port number */
                        memset(token, 0, sizeof (token));
                        registered[j].p = atoi(strtok(NULL, delim));
                        memset(token, 0, sizeof (token));
                        break;
                    }
                }

                /* clear contents in variables */
                memset(read_u_l, 0, sizeof (read_u_l));
                memset(read_l_l, 0, sizeof (read_l_l));
                j++;
            }

            /* close users and locations file and send users information to client */
            fclose(users_file);
            fclose(locations_file);
            write(client_sock_no, registered, sizeof (registered));

            // </editor-fold>

            puts("... Requested list sent to client\n");
            printf("... Total number of users online: %d\n\n", total_online_users);
            break;
            // </editor-fold>

        case USAGE_UNR:

            // <editor-fold defaultstate="collapsed" desc="unregister logic">
            puts("Client seeking to be unregistered");

            /* open the file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            j = 0;
            const char del_r[2] = "|";
            memset(registered, 0, sizeof (registered));

            for (;;) {
                if ((fscanf(users_file, "%s\n", read_l_l)) != 1) {
                    break;
                }

                /* get the username */
                token = strtok(read_l_l, del_r);
                if ((strcmp(reg_inf[0].username, token)) != 0) {
                    /* write the username */
                    strncpy(registered[j].username, token, strlen(token));

                    /* walk through the next token; ip address */
                    token = strtok(NULL, del_r);
                    strncpy(registered[j].password, token, strlen(token));

                    memset(token, 0, sizeof (token));
                    j++;
                }
            }

            /* close the file */
            fclose(users_file);

            /* open client registration file */
            users_file = fopen(users_file_n, "w");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for writing.\n");
                exit(EXIT_FAILURE);
            }

            /* update clients in file */
            i = 0;
            total_online_users = 0;
            while (i < j) {
                fprintf(users_file, "%s|", registered[i].username);
                fprintf(users_file, "%s|\n", registered[i].password);
                i++, total_online_users++;
            }

            /* close the file */
            fclose(users_file);

            memset(read_l_l, 0, sizeof (read_l_l));

            /* open client registration file */
            locations_file = fopen(locations_file_n, "r");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }
            j = 0;
            char delr[2] = "|";
            for (;;) {
                if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                    break;
                }

                /* get the username */
                token = strtok(read_l_l, delr);
                if ((strcmp(reg_inf[0].username, token)) != 0) {
                    /* write the username */
                    strncpy(registered[j].username, token, strlen(token));

                    /* walk through the next token; ip address */
                    token = strtok(NULL, delr);
                    strncpy(registered[j].ip, token, strlen(token));

                    /* walk through the next token; port number */
                    memset(token, 0, sizeof (token));
                    registered[j].p = atoi(strtok(NULL, delr));
                    memset(token, 0, sizeof (token));
                    j++;
                }
            }

            fclose(locations_file);

            /* open client registration file */
            locations_file = fopen(locations_file_n, "w");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for writing.\n");
                exit(EXIT_FAILURE);
            }

            /* update clients locations in file */

            i = 0;
            while (i < j) {
                fprintf(locations_file, "%s|", registered[i].username);
                fprintf(locations_file, "%s|", registered[i].ip);
                fprintf(locations_file, "%d\n", registered[i].p);
                i++;
            }

            /* close the files */
            fclose(locations_file);

            // <editor-fold defaultstate="collapsed" desc="read the list of users">

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* open the locations file for reading */
            locations_file = fopen(locations_file_n, "r");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* clear the content in registered data structure */
            memset(registered, 0, sizeof (registered));
            memset(read_u_l, 0, sizeof (read_u_l));
            memset(read_l_l, 0, sizeof (read_l_l));

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* create the users information */
            j = 0;
            for (;;) {
                if ((fscanf(users_file, "%s\n", read_u_l)) != 1) {
                    break;
                }

                const char delim[2] = "|";
                char *token;

                /* get the username */
                token = strtok(read_u_l, delim);
                strncpy(registered[j].username, token, strlen(token));

                /* get the password */
                token = strtok(NULL, delim);
                strncpy(registered[j].password, token, strlen(token));

                char f_l[50];
                char *friends;

                /* delete the memory holding friends list */
                memset(registered[j].friends_uns, 0, sizeof (registered[j].friends_uns));

                /* get the friends */
                friends = strtok(NULL, delim);
                if (friends) {
                    strncpy(f_l, friends, strlen(friends));

                    const char del[2] = ";";
                    char *tok;
                    int k = 0;
                    tok = strtok(f_l, del);
                    while (tok) {

                        /* get each friend */
                        int w = 0, f_exists = FALSE;
                        if (strcmp(registered[i].username, tok) != 0) {
                            for (w = 0; w < 10; w++) {
                                if (strcmp(registered[j].friends_uns[w], "") == 0) break;
                                else if (strcmp(registered[j].friends_uns[w], tok) == 0) {
                                    f_exists = TRUE;
                                    break;
                                }
                            }
                        } else f_exists = TRUE;

                        if (!f_exists) strncpy(registered[j].friends_uns[k], tok, strlen(tok));

                        k++;

                        tok = strtok(NULL, del);
                    }
                }

                for (;;) {
                    if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                        break;
                    }

                    /* get the username */
                    token = strtok(read_l_l, delim);
                    if ((strcmp(registered[j].username, token)) == 0) {

                        /* walk through the next token; ip address */
                        token = strtok(NULL, delim);
                        strncpy(registered[j].ip, token, strlen(token));

                        /* walk through the next token; port number */
                        memset(token, 0, sizeof (token));
                        registered[j].p = atoi(strtok(NULL, delim));
                        memset(token, 0, sizeof (token));
                        break;
                    }
                }

                /* clear contents in variables */
                memset(read_u_l, 0, sizeof (read_u_l));
                memset(read_l_l, 0, sizeof (read_l_l));
                j++;
            }

            /* close users and locations file and send users information to client */
            fclose(users_file);
            fclose(locations_file);
            write(client_sock_no, registered, sizeof (registered));

            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="create conn to clients and send list of online users ">
            i = 0;
            for (;;) {
                if (strcmp(registered[i].username, reg_inf[0].username) == 0) {
                    i++;
                    continue;
                }
                if (registered[i].p == 0) {
                    break;
                }

                /* Sock used in creating conn to client */
                sock_d_to_create_c_conn = socket(AF_INET, SOCK_STREAM, 0);
                if (sock_d_to_create_c_conn == -1) {
                    printf("\nCould not create socket");
                }

                /* Initialize random number generator */
                srand((unsigned) time(NULL));

                /* Generate a random port number */
                int port = rand() % 7000 + 70000;

                /* Details of socket to create conn to client */
                memset(&this_server, 0, sizeof (struct sockaddr_in));
                this_server.sin_family = AF_INET;
                this_server.sin_port = htons(port);
                this_server.sin_addr.s_addr = INADDR_ANY;

                /* Details for client socket */
                memset(&client, 0, sizeof (struct sockaddr_in));
                client.sin_family = AF_INET;
                client.sin_port = htons(registered[i].p);
                client.sin_addr.s_addr = inet_addr(registered[i].ip);

                /* Connect to the client */
                if (connect(sock_d_to_create_c_conn, (struct sockaddr *) &client, sizeof (client)) < 0) {
                    perror("connect failed. Error");
                    exit(EXIT_FAILURE);
                }

                /* prepare the invite message */
                msg_to_client[0].type = CLIENTS;

                /* send the invite message to client */
                write(sock_d_to_create_c_conn, msg_to_client, sizeof (msg_to_client));

                /* send list of online users */
                write(sock_d_to_create_c_conn, registered, sizeof (registered));

                /* close connection to client */
                close(sock_d_to_create_c_conn);
                i++;
            }
            // </editor-fold>

            puts("... Client unregistered\n");
            printf("... Total number of users online: %d\n\n", total_online_users);
            break;
            // </editor-fold>

        case USAGE_LOG_OUT:

            // <editor-fold defaultstate="collapsed" desc="logout logic">
            puts("Client seeking to be logged out");

            j = 0;

            memset(registered, 0, sizeof (registered));
            memset(read_l_l, 0, sizeof (read_l_l));

            /* open client registration file */
            locations_file = fopen(locations_file_n, "r");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            const char del[2] = "|";
            j = 0;
            total_online_users = 0;
            for (;;) {
                if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                    break;
                }

                /* get the username */
                token = strtok(read_l_l, del);
                if ((strcmp(reg_inf[0].username, token)) != 0) {
                    /* write the username */
                    strncpy(registered[j].username, token, strlen(token));

                    /* walk through the next token; ip address */
                    token = strtok(NULL, del);
                    strncpy(registered[j].ip, token, strlen(token));

                    /* walk through the next token; port number */
                    registered[j].p = atoi(strtok(NULL, del));

                    j++, total_online_users++;
                }
            }

            fclose(locations_file);

            /* open client registration file */
            locations_file = fopen(locations_file_n, "w");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for writing.\n");
                exit(EXIT_FAILURE);
            }

            /* update clients locations in file */
            i = 0;
            while (i < j) {
                if (strlen(registered[i].username) > 0) {
                    fprintf(locations_file, "%s|", registered[i].username);
                    fprintf(locations_file, "%s|", registered[i].ip);
                    fprintf(locations_file, "%d\n", registered[i].p);
                }
                i++;
            }

            /* close the files */
            fclose(locations_file);

            // <editor-fold defaultstate="collapsed" desc="read the list of users">

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* open the locations file for reading */
            locations_file = fopen(locations_file_n, "r");
            if (locations_file == NULL) {
                perror("Couldn't open locations_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* clear the content in registered data structure */
            memset(registered, 0, sizeof (registered));
            memset(read_u_l, 0, sizeof (read_u_l));
            memset(read_l_l, 0, sizeof (read_l_l));

            /* open the users file for reading */
            users_file = fopen(users_file_n, "r");
            if (users_file == NULL) {
                perror("Couldn't open users_file_n for reading.\n");
                exit(EXIT_FAILURE);
            }

            /* create the users information */
            j = 0;
            for (;;) {
                if ((fscanf(users_file, "%s\n", read_u_l)) != 1) {
                    break;
                }

                const char delim[2] = "|";
                char *token;

                /* get the username */
                token = strtok(read_u_l, delim);
                strncpy(registered[j].username, token, strlen(token));

                /* get the password */
                token = strtok(NULL, delim);
                strncpy(registered[j].password, token, strlen(token));

                char f_l[50];
                char *friends;

                /* delete the memory holding friends list */
                memset(registered[j].friends_uns, 0, sizeof (registered[j].friends_uns));

                /* get the friends */
                friends = strtok(NULL, delim);
                if (friends) {
                    strncpy(f_l, friends, strlen(friends));

                    const char del[2] = ";";
                    char *tok;
                    int k = 0;
                    tok = strtok(f_l, del);
                    while (tok) {

                        /* get each friend */
                        int w = 0, f_exists = FALSE;
                        if (strcmp(registered[i].username, tok) != 0) {
                            for (w = 0; w < 10; w++) {
                                if (strcmp(registered[j].friends_uns[w], "") == 0) break;
                                else if (strcmp(registered[j].friends_uns[w], tok) == 0) {
                                    f_exists = TRUE;
                                    break;
                                }
                            }
                        } else f_exists = TRUE;

                        if (!f_exists) strncpy(registered[j].friends_uns[k], tok, strlen(tok));

                        k++;

                        tok = strtok(NULL, del);
                    }
                }

                for (;;) {
                    if ((fscanf(locations_file, "%s\n", read_l_l)) != 1) {
                        break;
                    }

                    /* get the username */
                    token = strtok(read_l_l, delim);
                    if ((strcmp(registered[j].username, token)) == 0) {

                        /* walk through the next token; ip address */
                        token = strtok(NULL, delim);
                        strncpy(registered[j].ip, token, strlen(token));

                        /* walk through the next token; port number */
                        memset(token, 0, sizeof (token));
                        registered[j].p = atoi(strtok(NULL, delim));
                        memset(token, 0, sizeof (token));
                        break;
                    }
                }

                /* clear contents in variables */
                memset(read_u_l, 0, sizeof (read_u_l));
                memset(read_l_l, 0, sizeof (read_l_l));
                j++;
            }

            /* close users and locations file and send users information to client */
            fclose(users_file);
            fclose(locations_file);
            write(client_sock_no, registered, sizeof (registered));

            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="create conn to clients and send list of online users ">
            i = 0;
            for (;;) {
                if (strcmp(registered[i].username, reg_inf[0].username) == 0) {
                    i++;
                    continue;
                }
                if (registered[i].p == 0) {
                    break;
                }

                /* Sock used in creating conn to client */
                sock_d_to_create_c_conn = socket(AF_INET, SOCK_STREAM, 0);
                if (sock_d_to_create_c_conn == -1) {
                    printf("\nCould not create socket");
                }

                /* Initialize random number generator */
                srand((unsigned) time(NULL));

                /* Generate a random port number */
                int port = rand() % 7000 + 70000;

                /* Details of socket to create conn to client */
                memset(&this_server, 0, sizeof (struct sockaddr_in));
                this_server.sin_family = AF_INET;
                this_server.sin_port = htons(port);
                this_server.sin_addr.s_addr = INADDR_ANY;

                /* Details for client socket */
                memset(&client, 0, sizeof (struct sockaddr_in));
                client.sin_family = AF_INET;
                client.sin_port = htons(registered[i].p);
                client.sin_addr.s_addr = inet_addr(registered[i].ip);

                /* Connect to the client */
                if (connect(sock_d_to_create_c_conn, (struct sockaddr *) &client, sizeof (client)) < 0) {
                    perror("connect failed. Error");
                    exit(EXIT_FAILURE);
                }

                /* prepare the invite message */
                msg_to_client[0].type = CLIENTS;

                /* send the invite message to client */
                write(sock_d_to_create_c_conn, msg_to_client, sizeof (msg_to_client));

                /* send list of online users */
                write(sock_d_to_create_c_conn, registered, sizeof (registered));

                /* close connection to client */
                close(sock_d_to_create_c_conn);
                i++;
            }
            // </editor-fold>

            puts("... Client logged out\n");
            printf("... Total number of users online: %d\n\n", total_online_users);

            // </editor-fold>

    }

    close(client_sock_no);
    free(socket_desc);

}
